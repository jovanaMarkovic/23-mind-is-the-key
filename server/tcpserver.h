#ifndef TCPSERVER_H
#define TCPSERVER_H

#include "questionslist.h"

#include <QDataStream>
#include <QDebug>
#include <QIODevice>
#include <QMap>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTimer>
#include <QVector>

class TcpServer : public QObject
{
	Q_OBJECT

public:
	explicit TcpServer(QObject *parent = nullptr);
	~TcpServer();
	void startServer();
	void sendMessageToClients(QByteArray message);

public slots:
	void newClientConnection();
	void on_timer();

	void socketDisconnected();
	void socketReadyRead();
	void socketStateChanged(QAbstractSocket::SocketState state);

private:
	QTcpServer *m_server;
	QVector<QString> m_names;
	QMap<QTcpSocket *, int> m_clients;

	QTimer *start_timer;
	const int timer_length = 5; // time for waiting until start in seconds
	int current_time = timer_length;

	bool should_start = false;
	bool game_started = false;

	void start_game();
	void start_turn();
	void prepare_start();
	int turn = -1;
	QDataStream in;

	QuestionsList *ql;
	int last_question = 0;
};

#endif // TCPSERVER_H
