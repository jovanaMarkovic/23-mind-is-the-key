#ifndef CONSTANTS_H
#define CONSTANTS_H

class Constants
{
public:
	// client sends name to server, and if it is first player schedules the game
	static const int CLIENT_SEND_NAME = 1;
	// sends name of players in order in which they join
	static const int SERVER_START_GAME = 2;
	// server sends questions to the player who is next
	static const int QUESTIONS = 3;
	// server sends notifications to players who should wait(they are not next)
	static const int WAIT_TURN = 4;

	// client sends and server forwards to move player
	static const int MOVE_PLAYER = 5;
	// client says that he finished his turn
	static const int END_TURN = 6;
	// client open card
	static const int OPEN_CARD = 7;

	// server notifies you when the game starts
	static const int TIME_UNTIL_START = 8;
	// time is up, there are not enough players
	static const int NOT_ENOUGH_PLAYERS = 9;
	// more than 4 players applied
	static const int TOO_MANY_PLAYERS = 10;
	// client disconnected from game-stop the game
	static const int CLIENT_DISCONNECTED = 11;

	// Cards
	static const int CARD_TRAVEL = 1;
	static const int CARD_MONEY = 2;
	static const int CARD_LOSE_ALL = 3;
	static const int CARD_LOSE_MONEY = 4;
	static const int CARD_SWEETS = 5;
	static const int CARD_FORWARD = 6;
	static const int CARD_BACKWARD = 7;
	static const int CARD_IMMUNITY = 8;
};

#endif // CONSTANTS_H
