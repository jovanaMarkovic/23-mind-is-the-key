#!/bin/bash
find ./client -regex '.*\.\(cpp\|h\)' -exec clang-format -style=file -i {} \;
find ./server -regex '.*\.\(cpp\|h\)' -exec clang-format -style=file -i {} \;

