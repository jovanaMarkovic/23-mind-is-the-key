#include "headers/bad_card.h"

#include <QString>
#include <algorithm>
#include <iostream>
#include <vector>

BadCard::BadCard(const std::string &name, const std::string &text, const std::vector<BadType> &types)
	: Card(name), card_text(text), card_types(types)
{
}

BadCard::~BadCard()
{
}

std::string BadCard::Type() const
{
	std::string out = "[";
	for (BadType t : card_types) {
		switch (t) {
		case BadType::LoseMoney:
			out += " LoseMoney";
			break;
		case BadType::LoseAll:
			out += " LoseAll";
			break;
		case BadType::HalfMoney:
			out += " HalfMoney";
			break;

		case BadType::MoveBackwards:
			out += " MoveBackwards";
			break;
		}
	}
	out += " ]";
	return out;
}

std::string BadCard::show() const
{
	return Card::show() + "\n" + Type();
}

void BadCard::set_card_text(std::string text)
{
	card_text = text;
}

std::string BadCard::get_card_text()
{
	return card_text;
}
