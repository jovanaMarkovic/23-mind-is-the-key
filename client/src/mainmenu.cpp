#include "headers/mainmenu.h"

#include "headers/mainwindow.h"
#include "headers/serverlogin.h"
#include "ui_mainmenu.h"

MainMenu::MainMenu(QWidget *parent) : QDialog(parent), ui(new Ui::MainMenu)
{
	ui->setupUi(this);
	set_background_image(":images/images/map3.png");
}

MainMenu::~MainMenu()
{
	delete ui;
}

void MainMenu::on_join_clicked()
{
	hide();
	ServerLogin *w = new ServerLogin;
	w->show();
	close();
}

void MainMenu::on_local_clicked()
{
	hide();
	MainWindow *w = new MainWindow;
	w->show();
	close();
}

void MainMenu::on_quit_clicked()
{
	close();
}

void MainMenu::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
