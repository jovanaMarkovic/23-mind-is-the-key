#include "headers/questionsbox.h"

#include "headers/die.h"
#include "headers/mainwindow.h"
#include "headers/question.h"
#include "ui_questionsbox.h"

#include <QDateTime>
#include <QDialog>
#include <QList>
#include <QMessageBox>
#include <QTimer>

QuestionsBox::QuestionsBox(QList<Question *> list, QWidget *parent) : QDialog(parent), ui(new Ui::QuestionsBox)
{

	QTimer *timer1 = new QTimer(this);
	timer2 = new QTimer(this);
	connect(timer1, SIGNAL(timeout()), this, SLOT(on_ok_button_clicked()));
	connect(timer2, SIGNAL(timeout()), this, SLOT(show_time()));
	timer1->start(30000);
	timer2->start(1000);

	ui->setupUi(this);
	set_background_image(":images/images/old.png");
	setWindowTitle("Questions");
	questions_list = list;

	n_questions = list.size();

	question_boxes = {ui->textBrowser,	 ui->textBrowser_2, ui->textBrowser_3,
					  ui->textBrowser_4, ui->textBrowser_5, ui->textBrowser_6};

	answer_boxes = {ui->lineEdit, ui->lineEdit_2, ui->lineEdit_3, ui->lineEdit_4, ui->lineEdit_5, ui->lineEdit_6};

	labels = {ui->lbl_tf1, ui->lbl_tf2, ui->lbl_tf3, ui->lbl_tf4, ui->lbl_tf5, ui->lbl_tf6};

	for (int i = 0; i < n_questions; i++) {
		question_boxes[i]->setText(QString::fromStdString(questions_list[i]->get_question()));
	}
}

QuestionsBox::~QuestionsBox()
{
	delete ui;
}
void QuestionsBox::show_time()
{
	time--;
	if (time == 0) {
		timer2->stop();
	}
	QString time_text = QString ::number(time);
	ui->lbl_seconds->setText(time_text);
}
void QuestionsBox::on_ok_button_clicked()
{

	QPixmap true_fig(":images/images/true.png");
	QPixmap false_fig(":images/images/false.png");

	for (int i = 0; i < n_questions; i++) {
		if (answer_boxes[i]->text().toLower() ==
			QString::fromStdString(questions_list[i]->get_answer()).toLower()) {
			true_answers++;
			labels[i]->setPixmap(true_fig);
		} else {
			labels[i]->setPixmap(false_fig);
		}
		labels[i]->show();
	}

	ui->ok_button->setEnabled(false);
}

void QuestionsBox::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}

int QuestionsBox::get_die()
{
	return n_questions;
}
int QuestionsBox::get_true_answers()
{
	return true_answers;
}
