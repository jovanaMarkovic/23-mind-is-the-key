#include "headers/help.h"

#include "ui_help.h"

#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextBrowser>
#include <QTextStream>
#include <iostream>

Help::Help(QWidget *parent) : QDialog(parent), ui(new Ui::Help)
{
	ui->setupUi(this);
	this->setFixedSize(400, 300);

	setWindowTitle("Help");
	set_background_image(":images/images/old.png");
}
Help::~Help()
{
	delete ui;
}
void Help::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
