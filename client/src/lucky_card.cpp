#include "headers/lucky_card.h"

#include <QString>
#include <algorithm>
#include <iostream>
#include <vector>

LuckyCard::LuckyCard(const std::string &name, const std::string &text, const std::vector<LuckyType> &types)
	: Card(name), card_text(text), card_types(types)
{
}

LuckyCard::~LuckyCard()
{
}

std::string LuckyCard::Type() const
{
	std::string out = "[";
	for (LuckyType t : card_types) {
		switch (t) {
		case LuckyType::Travelling:
			out += " Travelling";
			break;
		case LuckyType::Sweets:
			out += " Sweets";
			break;
		case LuckyType::Money:
			out += " Money";
			break;
		case LuckyType::MoveForwards:
			out += " MoveForward";
			break;
		case LuckyType::Immunity:
			out += " Immunity";
			break;
		}
	}
	out += " ]";
	return out;
}

std::string LuckyCard::show() const
{
	return Card::show() + "\n" + Type();
}

void LuckyCard::set_card_text(std::string text)
{
	card_text = text;
}

std::string LuckyCard::get_card_text()
{
	return card_text;
}
