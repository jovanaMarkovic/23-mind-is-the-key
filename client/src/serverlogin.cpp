#include "headers/serverlogin.h"

#include "constants.h"
#include "headers/servergame.h"
#include "ui_serverlogin.h"

ServerLogin::ServerLogin(QWidget *parent) : QDialog(parent), ui(new Ui::ServerLogin)
{
	ui->setupUi(this);
	set_background_image(":images/images/paper1.png");
	ui->startingIn->hide();
	connectedToHost = false;
}

ServerLogin::~ServerLogin()
{
	delete ui;
}

void ServerLogin::on_startGame_clicked()
{
	ui->lbl_error->text().clear();
	if (ui->name->text().isEmpty()) {
		ui->lbl_error->setText("Enter your name!");
		return;
	}

	if (!connectedToHost)

	{
		ui->startGame->setEnabled(false);
		socket = new QTcpSocket();

		connect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
		connect(socket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
		connect(socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()));

		socket->connectToHost("127.0.0.1", 8001);
	}
}

void ServerLogin::socketConnected()
{
	qDebug() << "Connected to server.";

	QString name = ui->name->text();

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_9);

	out << Constants::CLIENT_SEND_NAME << name.toUtf8();

	socket->write(block);

	connectedToHost = true;

	in.setDevice(socket);
	in.setVersion(QDataStream::Qt_5_9);
}

void ServerLogin::socketDisconnected()
{
	qDebug() << "Disconnected from server. ";
	connectedToHost = false;
}

void ServerLogin::socketReadyRead()
{
	qDebug() << "read";
	in.startTransaction();

	int type;
	in >> type;
	qDebug() << QString::fromStdString(std::to_string(type));

	if (type == Constants::SERVER_START_GAME) {
		int number_players;
		in >> number_players;
		QVector<QString> players;
		for (int i = 0; i < number_players; i++) {
			QByteArray name;
			in >> name;
			players.push_back(QString::fromUtf8(name));
		}

		disconnect(socket, SIGNAL(connected()), this, SLOT(socketConnected()));
		disconnect(socket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
		disconnect(socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()));

		qDebug() << "start game";
		ServerGame *w = new ServerGame(socket, players);
		w->show();
		hide();
	} else if (type == Constants::TIME_UNTIL_START) {
		int time;
		in >> time;
		ui->startingIn->show();
		ui->startingIn->setText("Starting in: " + QString::number(time));
	} else if (type == Constants::NOT_ENOUGH_PLAYERS) {
		ui->startingIn->show();
		ui->startingIn->setText("Not enough players, waiting again");
	} else if (type == Constants::TOO_MANY_PLAYERS) {
		ui->startingIn->show();
		ui->startingIn->setText("There are already 4 players");
		socket->disconnectFromHost();
		ui->startGame->setEnabled(true);
	} else {
		qDebug() << "unknown commnad " << type;
	}

	if (!in.commitTransaction())
		return;
}

void ServerLogin::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
