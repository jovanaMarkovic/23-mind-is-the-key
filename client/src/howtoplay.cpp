#include "headers/howtoplay.h"

#include "ui_howtoplay.h"

#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextBrowser>
#include <QTextStream>
#include <iostream>

HowToPlay::HowToPlay(QWidget *parent) : QDialog(parent), ui(new Ui::HowToPlay)
{
	ui->setupUi(this);
	this->setFixedSize(400, 300);

	setWindowTitle("Settings");
	set_background_image(":images/images/old.png");

	QString text;
	int number_of_lines = 0;
}

HowToPlay::~HowToPlay()
{
	delete ui;
}

void HowToPlay::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
