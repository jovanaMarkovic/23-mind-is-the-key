#include "headers/servergame.h"

#include "constants.h"

#include <QMessageBox>

ServerGame::ServerGame(QTcpSocket *socket, QVector<QString> players, QWidget *parent)
	: QMainWindow(parent), socket(socket), ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	this->setWindowTitle("Mind is the key");

	this->setFixedSize(1250, 604);

	game = new Game(players);

	// Fill in the names for 4 players
	for (int i = players.size(); i < 4; i++)
		players.push_back("");

	QString p1_name = players[0];
	QString p2_name = players[1];
	QString p3_name = players[2];
	QString p4_name = players[3];

	ui->player_name1->setText(p1_name);
	ui->player_name2->setText(p2_name);
	ui->player_name3->setText(p3_name);
	ui->player_name4->setText(p4_name);

	set_space_list();
	this->player_list = game->get_player_list();
	this->update_display();

	money_labels = {ui->money1, ui->money2, ui->money3, ui->money4};
	travel_labels = {ui->travel1, ui->travel2, ui->travel3, ui->travel4};
	sweets_labels = {ui->sweet1, ui->sweet2, ui->sweet3, ui->sweet4};
	immunity_labels = {ui->immunity1, ui->immunity2, ui->immunity3, ui->immunity4};

	in.setDevice(socket);
	in.setVersion(QDataStream::Qt_5_9);
	connect(socket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
	connect(socket, SIGNAL(readyRead()), this, SLOT(socketReadyRead()));
}

ServerGame::~ServerGame()
{
	delete ui;
}

void ServerGame::socketDisconnected()
{
	qDebug() << "Disconnected from server. ";
}

void ServerGame::socketReadyRead()
{
	in.startTransaction();

	int type;
	in >> type;
	qDebug() << QString::fromStdString(std::to_string(type));

	if (type == Constants::QUESTIONS) {
		int number_questions;
		in >> number_questions;
		questions_list.clear();
		for (int i = 0; i < number_questions; i++) {
			QByteArray question;
			in >> question;
			QByteArray answer;
			in >> answer;
			questions_list.push_back(
				new Question(QString::fromUtf8(question).toStdString(), QString::fromUtf8(answer).toStdString()));
		}
		ui->question_button->setEnabled(true);
	} else if (type == Constants::WAIT_TURN) {
		update_display();

		ui->question_button->setDisabled(true);
		ui->end_roll_button->setDisabled(true);
		ui->label_card->setText(QString::fromStdString(""));
		ui->card_button->setStyleSheet("border-image:url(:images/images/card.png);");
		ui->card_button->setEnabled(false);
		ui->roll_button->setEnabled(false);
	} else if (type == Constants::MOVE_PLAYER) {
		int number;
		in >> number;

		game->move_player(number);
		update_display();
	} else if (type == Constants::OPEN_CARD) {
		int card_type;
		in >> card_type;

		Player *currentPlayer = game->get_current_player();

		int amount;
		if (card_type == Constants::CARD_TRAVEL) {
			in >> amount;
			currentPlayer->set_travell(amount);
			travel_labels[currentPlayer->get_number() - 1]->setText(QString::number(currentPlayer->get_travell()));
		} else if (card_type == Constants::CARD_MONEY) {
			in >> amount;
			currentPlayer->set_money_amount(amount);
			money_labels[currentPlayer->get_number() - 1]->setText(
				QString::number(currentPlayer->get_money_amount()));
		} else if (card_type == Constants::CARD_SWEETS) {
			in >> amount;
			currentPlayer->set_sweets(amount);
			sweets_labels[currentPlayer->get_number() - 1]->setText(QString::number(currentPlayer->get_sweets()));
		} else if (card_type == Constants::CARD_IMMUNITY) {
			in >> amount;
			currentPlayer->set_ind_immunity(amount);
			immunity_labels[currentPlayer->get_number() - 1]->setText(
				QString::number(currentPlayer->get_ind_immunity()));
		} else if (card_type == Constants::CARD_LOSE_MONEY) {
			currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
			money_labels[currentPlayer->get_number() - 1]->setText(
				QString::number(currentPlayer->get_money_amount()));
		} else if (card_type == Constants::CARD_LOSE_ALL) {
			if (currentPlayer->get_ind_immunity() == 0) {
				currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
				currentPlayer->set_sweets(-currentPlayer->get_sweets());
				currentPlayer->set_travell(-currentPlayer->get_travell());
				money_labels[currentPlayer->get_number() - 1]->setText(
					QString::number(currentPlayer->get_money_amount()));
				travel_labels[currentPlayer->get_number() - 1]->setText(
					QString::number(currentPlayer->get_travell()));
				sweets_labels[currentPlayer->get_number() - 1]->setText(
					QString::number(currentPlayer->get_sweets()));
			}

		} else if (card_type == Constants::CARD_FORWARD) {
			game->move_player(1);
		} else if (card_type == Constants::CARD_BACKWARD) {
			if (currentPlayer->get_position() != 0)
				game->move_player(-1);
		}

		update_display();
	} else if (type == Constants::END_TURN) {
		game->endturn();
		update_display();
	} else if (type == Constants::CLIENT_DISCONNECTED) {
		QMessageBox::critical(this, "Player disconnected", "A player has disconnected, stopping game");
		socket->disconnectFromHost();
		close();
	} else {
		qDebug() << "unknown commnad " << type;
	}

	if (!in.commitTransaction())
		return;
}

void ServerGame::update_display()
{

	// reset the board (clears every space)
	for (int i = 0; i < space_list.length(); i++) {
		space_list[i]->setStyleSheet("");
		space_list[i]->clear();
	}

	// update board
	Player *current_player = game->get_current_player();

	for (int j = 0; j < player_list.length(); j++) {
		int index = game->get_player_position(player_list[j]);
		// highlight current player position
		if (player_list[j] == current_player) {
			space_list[index]->setStyleSheet("background: rgba(255, 212, 128, .4)");
		}

		switch (player_list[j]->get_piece()) {
		case 1:
			space_list[index]->setPixmap(QPixmap(":images/images/PhilipMartin.png"));
			break;
		case 2:
			space_list[index]->setPixmap(QPixmap(":images/images/Columbus.png"));
			break;
		case 3:
			space_list[index]->setPixmap(QPixmap(":images/images/Explorer.png"));
			break;
		case 4:
			space_list[index]->setPixmap(QPixmap(":images/images/Magellan.png"));
			break;
		}
	}

	// update current player
	ui->lbl_player->setText(QString::fromStdString(current_player->get_player_name()) + "'s turn");
}

bool label_less_than2(QLabel *L1, QLabel *L2)
{
	// sort name label in set_space_list()
	return L1->objectName() < L2->objectName();
}

void ServerGame::set_space_list()
{
	//- - -
	//-   -
	//- - 0 (0-start)
	QList<QLabel *> list = ui->Board->findChildren<QLabel *>();
	std::sort(list.begin(), list.end(), label_less_than2);
	list.takeFirst();
	this->space_list = list;
}

void ServerGame::on_roll_button_clicked()
{
	int number = true_answers;

	game->move_player(number);
	update_display();

	ui->roll_button->setEnabled(false);
	ui->end_roll_button->setEnabled(true);
	ui->card_button->setEnabled(true);

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_9);

	out << Constants::MOVE_PLAYER << number;

	socket->write(block);
}

void ServerGame::on_end_roll_button_clicked()
{
	game->endturn();
	update_display();

	ui->end_roll_button->setDisabled(true);
	ui->label_card->setText(QString::fromStdString(""));
	ui->card_button->setStyleSheet("border-image:url(:images/images/card.png);");
	ui->card_button->setEnabled(false);
	ui->roll_button->setEnabled(false);

	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_9);

	out << Constants::END_TURN;

	socket->write(block);
}

void ServerGame::on_question_button_clicked()
{
	QuestionsBox questionsbox(this->questions_list);
	questionsbox.setModal(true);
	questionsbox.exec();

	true_answers = questionsbox.get_true_answers();

	switch (true_answers) {
	case 0: {
		ui->label->setPixmap(QPixmap(":images/images/die0.png"));
	} break;
	case 1: {
		ui->label->setPixmap(QPixmap(":images/images/die1.png"));
	} break;
	case 2: {
		ui->label->setPixmap(QPixmap(":images/images/die2.png"));
	} break;
	case 3: {
		ui->label->setPixmap(QPixmap(":images/images/die3.png"));
	} break;
	case 4: {
		ui->label->setPixmap(QPixmap(":images/images/die4.png"));
	} break;
	case 5: {
		ui->label->setPixmap(QPixmap(":images/images/die5.png"));
	} break;
	case 6: {
		ui->label->setPixmap(QPixmap(":images/images/die6.png"));
	} break;
	}

	ui->roll_button->setEnabled(true);
}

void ServerGame::on_card_button_clicked()
{
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_9);
	out << Constants::OPEN_CARD;

	Player *currentPlayer = game->get_current_player();

	int random = rand() % 2 + 1;
	if (random == 1) {
		std::vector<LuckyType> v2 {LuckyType::Travelling, LuckyType::Sweets, LuckyType::Money,
								   LuckyType::MoveForwards, LuckyType::Immunity};

		LuckyCard *luck1t = new LuckyCard("n1", "You got a travel voucher to Paris!", v2);

		LuckyCard *luck2t = new LuckyCard("n1", "You got a travel voucher to Rome!", v2);

		LuckyCard *luck3t = new LuckyCard("n1", "You got a travel voucher to Cuba!", v2);

		LuckyCard *luck4t = new LuckyCard("n1", "You got a travel voucher to London!", v2);

		LuckyCard *luck5t = new LuckyCard("n1", "You got a travel voucher to Dubai!", v2);

		LuckyCard *luck1s = new LuckyCard("n2", "You got sweets", v2);

		LuckyCard *luck1m = new LuckyCard("n3", "You got money!", v2);

		LuckyCard *luck4 = new LuckyCard("n4", "Move forwards one step!", v2);

		LuckyCard *luck5 = new LuckyCard("n5", "You got immunity until the end of game!", v2);

		int value1 = rand() % 5;
		int c = rand() % 5;

		switch (v2[value1]) {
		case LuckyType::Travelling:
			switch (c) {
			case 0:
				ui->label_card->setText(QString::fromStdString(luck1t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:images/images/paris.png);");
				break;
			case 1:
				ui->label_card->setText(QString::fromStdString(luck2t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:images/images/rome.png);");
				break;
			case 2:
				ui->label_card->setText(QString::fromStdString(luck3t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:images/images/cuba.png);");
				break;
			case 3:
				ui->label_card->setText(QString::fromStdString(luck4t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:images/images/london.png);");
				break;
			case 4:
				ui->label_card->setText(QString::fromStdString(luck5t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:images/images/dubai.png);");
				break;
			}
			out << Constants::CARD_TRAVEL << 1;
			currentPlayer->set_travell(1);
			if (currentPlayer->get_number() == 1) {
				ui->travel1->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 2) {
				ui->travel2->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 3) {
				ui->travel3->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 4) {
				ui->travel4->setText(QString::number(currentPlayer->get_travell()));
			}
			break;
		case LuckyType::Sweets:
			ui->label_card->setText(QString::fromStdString(luck1s->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/Sweets.png);");
			out << Constants::CARD_SWEETS << 1;
			currentPlayer->set_sweets(1);
			if (currentPlayer->get_number() == 1) {
				ui->sweet1->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 2) {
				ui->sweet2->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 3) {
				ui->sweet3->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 4) {
				ui->sweet4->setText(QString::number(currentPlayer->get_sweets()));
			}
			break;
		case LuckyType::Money:
			ui->label_card->setText(QString::fromStdString(luck1m->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/Money.png);");
			if (currentPlayer->get_position() < 6) {
				out << Constants::CARD_MONEY << 100;
				currentPlayer->set_money_amount(100);
			} else if (currentPlayer->get_position() < 11) {
				out << Constants::CARD_MONEY << 500;
				currentPlayer->set_money_amount(500);
			} else if (currentPlayer->get_money_amount() < 17) {
				out << Constants::CARD_MONEY << 1000;
				currentPlayer->set_money_amount(1000);
			} else if (currentPlayer->get_money_amount() < 25) {
				out << Constants::CARD_MONEY << 2000;
				currentPlayer->set_money_amount(2000);
			}
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case LuckyType::MoveForwards:
			ui->label_card->setText(QString::fromStdString(luck4->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/MoveForwards.png);");
			out << Constants::CARD_FORWARD;
			currentPlayer->move_one_step();
			break;
		case LuckyType::Immunity:
			ui->label_card->setText(QString::fromStdString(luck5->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/Immunity.png);");
			out << Constants::CARD_IMMUNITY << 1;
			currentPlayer->set_ind_immunity(1);
			if (currentPlayer->get_number() == 1) {
				ui->immunity1->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 2) {
				ui->immunity2->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 3) {
				ui->immunity3->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 4) {
				ui->immunity4->setText(QString::number(currentPlayer->get_ind_immunity()));
			}
			break;
		}

	} else {
		int value2 = rand() % 4;

		std::vector<BadType> v {BadType::LoseMoney, BadType::LoseAll, BadType::HalfMoney, BadType::MoveBackwards};

		BadCard *bad1 = new BadCard("n1", "You lose all money!", v);

		BadCard *bad2 = new BadCard("n2", "You lose everything", v);

		BadCard *bad3 = new BadCard("n3", "You lose half of your money!", v);

		BadCard *bad4 = new BadCard("n4", "Move backwards one step!", v);

		switch (v[value2]) {
		case BadType::LoseMoney:
			ui->label_card->setText(QString::fromStdString(bad1->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/LoseMoney.png);");
			out << Constants::CARD_LOSE_MONEY;
			currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case BadType::LoseAll:
			ui->label_card->setText(QString::fromStdString(bad2->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/LoseAll.png);");
			out << Constants::CARD_LOSE_ALL;
			if (currentPlayer->get_ind_immunity() == 0) {
				currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
				currentPlayer->set_travell(-currentPlayer->get_travell());
				currentPlayer->set_sweets(-currentPlayer->get_sweets());
				if (currentPlayer->get_number() == 1) {
					ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel1->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet1->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 2) {
					ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel2->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet2->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 3) {
					ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel3->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet3->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 4) {
					ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel4->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet4->setText(QString::number(currentPlayer->get_sweets()));
				}
			}
			break;
		case BadType::HalfMoney:
			ui->label_card->setText(QString::fromStdString(bad3->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/HalfMoney.png);");
			out << Constants::CARD_MONEY << (-(currentPlayer->get_money_amount()) / 2);
			currentPlayer->set_money_amount(-(currentPlayer->get_money_amount()) / 2);
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case BadType::MoveBackwards:
			ui->label_card->setText(QString::fromStdString(bad4->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:images/images/MoveBackwards.png);");
			out << Constants::CARD_BACKWARD;
			if (currentPlayer->get_position() != 0) {
				currentPlayer->move_one_step_back();
			}
			break;
		}
	}
	update_display();

	socket->write(block);
}
