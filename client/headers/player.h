#ifndef PLAYER_H
#define PLAYER_H

#include <QApplication>
#include <cstdlib>
#include <string>

class Player
{
public:
	Player();
	Player(std::string player_name, std::string piece, int number, int money_amount);
	~Player();

	std::string get_player_name();
	int get_piece();
	int get_number();
	int get_money_amount();
	int get_ind_immunity();
	int get_travell();
	int get_sweets();
	unsigned int get_position();

	void set_number(int num);
	void set_player_name(std::string name);
	void set_piece(std::string piece);
	void set_money_amount(int money);
	void set_ind_immunity(int immunity);
	void set_travell(int num);
	void set_sweets(int num);
	void move_player(int num_fields);
	void move_one_step();
	void move_one_step_back();

	std::string to_string() const;
	void set_position(int number);

private:
	std::string m_player_name;
	int m_piece;
	int m_number;
	int m_money_amount = 0;
	int sweets = 0;
	int ind_immunity = 0;
	int travell = 0;
	unsigned int m_position;
};

#endif
