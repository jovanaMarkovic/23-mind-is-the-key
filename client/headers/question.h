#ifndef QUESTION_H
#define QUESTION_H

#include <iostream>
#include <string>
#include <vector>

class Question
{

public:
	Question();
	Question(std::string question_text, std::string answer) : m_question(question_text), m_answer(answer)
	{
	}
	~Question();

	void set_question(std::string question);
	void set_answer(std::string ans);

	std::string get_question();
	std::string get_answer();

private:
	std::string m_question;
	std::string m_answer;
};

#endif
