#ifndef LOGIN_H
#define LOGIN_H

#include <QDialog>
#include <QMediaPlayer>

namespace Ui
{
class Login;
}

class Login : public QDialog
{
	Q_OBJECT

public:
	explicit Login(QWidget *parent = nullptr);
	~Login();
	QString get_player_one();
	QString get_player_two();
	QString get_player_three();
	QString get_player_four();
	int get_players();

private slots:
	void on_button_clicked();
	void on_confirm_button_clicked();
	void on_mute_clicked();
	void on_unmute_clicked();

private:
	QMediaPlayer *musica;
	int players = 0;
	void set_background_image(QString image_path);
	Ui::Login *ui;
	QString player_one = "Philip Martin";
	QString player_two = "Columbus";
	QString player_three = "Explorer";
	QString player_four = "Magellan";
	bool ind_for_number = false;
	std::string p1;
	std::string p2;
	std::string p3;
	std::string p4;
};

#endif // LOGIN_H
