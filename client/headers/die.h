#ifndef DIE_H
#define DIE_H

class Die
{

private:
	int sides;

public:
	Die();
	int roll_die();
};

#endif
